package com.company;

public class Main {

    public static void main(String[] args) {

        isCatPlaying(false, 36);
    }

    public static boolean isCatPlaying(boolean summer, int temperature) {
        if ((summer && temperature <= 45 && temperature >= 25) || (!summer && temperature <= 35 && temperature >= 25))
        {
            System.out.println("Cat is playing");
            return true;
        } else {
            System.out.println("Cat is not playing");
            return false;
        }


    }
}